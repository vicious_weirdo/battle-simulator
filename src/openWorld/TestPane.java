package openWorld;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

public class TestPane extends JPanel {

	private int columnCount = 12;
	private int rowCount = 12;
	public ArrayList<Rectangle> cells;
	private Rectangle selectedCell;

	public TestPane() {
		cells = new ArrayList<>(columnCount * rowCount);

	}

	public Dimension getPreferredSize() {
		return new Dimension(700, 700);
	}

	public void invalidate() {
		cells.clear();
		super.invalidate();

	}

	public void keyPressed(KeyEvent e) {

		int hDirection = 0;
		int vDirection = 0;
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			hDirection -= 1;
		}
		if (key == KeyEvent.VK_RIGHT) {
			hDirection += 1;
		}
		if (key == KeyEvent.VK_UP) {
			vDirection += 1;
		}
		if (key == KeyEvent.VK_DOWN) {
			vDirection -= 1;
		}

		int column = 1;
		int row = 1;

		int width = getWidth();
		int height = getHeight();

		int cellWidth = width / columnCount;
		int cellHeight = height / rowCount;

		if (((hDirection != 0)) || ((vDirection != 0))) {
			column += vDirection;
			row += hDirection;
			vDirection -= 1;
			hDirection -= 1;
			selectedCell = new Rectangle(column, row, cellWidth, -(cellHeight));

		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();

		int width = getWidth();
		int height = getHeight();

		int cellWidth = width / columnCount;
		int cellHeight = height / rowCount;

		int xOffset = (width - (columnCount * cellWidth)) / 2;
		int yOffset = (height - (rowCount * cellHeight)) / 2;

		if (cells.isEmpty()) {
			for (int row = 0; row < rowCount; row++) {
				for (int col = 0; col < columnCount; col++) {
					Rectangle cell = new Rectangle(xOffset + (col * cellWidth), yOffset + (row * cellHeight), cellWidth,
							cellHeight);
					cells.add(cell);
				}
			}
		}

		g2d.setColor(Color.BLACK);
		for (Rectangle cell : cells)
			g2d.draw(cell);
		
		

		g2d.dispose();
	}
}