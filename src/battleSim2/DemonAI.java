package battleSim2;

import battleSim2.BuildUI;
import java.util.concurrent.ThreadLocalRandom;

public class DemonAI {
	public static void DemonAttack() {
		int DemonAtkChoice = ThreadLocalRandom.current().nextInt(1, 6 + 1);
		int modifier = 0;
		if (BuildUI.EnemyHP > 80) {
			modifier = 2;
		} else if (BuildUI.EnemyHP < 30) {
			modifier = -3;
		} else if (BuildUI.EnemyHP > 50) {
			modifier = 1;
		}

		switch (DemonAtkChoice + modifier) {
		case 1:
		case 2:
			System.out.println("Healing");
			BuildUI.EnemyHP += 20;
			BuildUI.enemyHPL.setText(" HP: " + BuildUI.EnemyHP);
			break;
		case 3:
		case 4:
			System.out.println("Weak Attack");
			BuildUI.UserHP -= 10;
			BuildUI.UserHPL.setText(" HP: " + BuildUI.UserHP);
			break;

		case 5:
		case 6:
			System.out.println("Strong attack");
			BuildUI.UserHP -= 30;
			BuildUI.UserHPL.setText(" HP: " + BuildUI.UserHP);
			break;
		}
	}

}
