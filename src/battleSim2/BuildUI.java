package battleSim2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class BuildUI extends JFrame {
	public static JLabel enemyL, enemyHPL, enemySpriteL, atkL, winL, UserL, UserHPL, UserManaL;
	public static JButton atk1Button, atk2Button, atk3Button, exitButton;
	public static int EnemyHP = 100;
	public static int UserHP = 100;
	public static int UserMana = 100;
	public static int enemyBurn = 0;
	public static boolean BossIsAlive = true;
	public static boolean flag = false;
	public static int y = 0;
	public static boolean AnimationOn = false;
	public static Timer timer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuildUI frame = new BuildUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuildUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 339);
		getContentPane().setLayout(new BorderLayout());
		setContentPane(new JLabel(new ImageIcon("Images/Backgrund.png")));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 };
		gridBagLayout.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		enemyL = new JLabel("Boss", SwingConstants.RIGHT);
		enemyL.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_enemyL = new GridBagConstraints();
		gbc_enemyL.insets = new Insets(0, 0, 5, 5);
		gbc_enemyL.gridx = 4;
		gbc_enemyL.gridy = 1;
		getContentPane().add(enemyL, gbc_enemyL);

		enemyHPL = new JLabel(" HP: " + EnemyHP, SwingConstants.LEFT);
		enemyHPL.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_enemyHPL = new GridBagConstraints();
		gbc_enemyHPL.insets = new Insets(0, 0, 5, 5);
		gbc_enemyHPL.gridx = 5;
		gbc_enemyHPL.gridy = 1;
		getContentPane().add(enemyHPL, gbc_enemyHPL);

		enemySpriteL = new JLabel("");
		enemySpriteL.setForeground(Color.LIGHT_GRAY);
		enemySpriteL.setIcon(new ImageIcon("Images/Demon1.png"));
		GridBagConstraints gbc_enemySpriteL = new GridBagConstraints();
		gbc_enemySpriteL.insets = new Insets(0, 0, 5, 5);
		gbc_enemySpriteL.gridx = 8;
		gbc_enemySpriteL.gridy = 3;
		getContentPane().add(enemySpriteL, gbc_enemySpriteL);

		atkL = new JLabel("Choose an attack: ", SwingConstants.LEFT);
		atkL.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 10;
		gbc_label.gridy = 5;
		getContentPane().add(atkL, gbc_label);

		UserL = new JLabel("User", SwingConstants.RIGHT);
		UserL.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_UserL = new GridBagConstraints();
		gbc_UserL.insets = new Insets(0, 0, 5, 5);
		gbc_UserL.gridx = 4;
		gbc_UserL.gridy = 8;
		getContentPane().add(UserL, gbc_UserL);

		UserHPL = new JLabel(" HP: " + UserHP, SwingConstants.LEFT);
		UserHPL.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_UserHPL = new GridBagConstraints();
		gbc_UserHPL.insets = new Insets(0, 0, 5, 5);
		gbc_UserHPL.gridx = 5;
		gbc_UserHPL.gridy = 8;
		getContentPane().add(UserHPL, gbc_UserHPL);

		UserManaL = new JLabel(" Mana: " + UserMana, SwingConstants.LEFT);
		UserManaL.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_UserManaL = new GridBagConstraints();
		gbc_UserManaL.insets = new Insets(0, 0, 5, 5);
		gbc_UserManaL.gridx = 5;
		gbc_UserManaL.gridy = 9;
		getContentPane().add(UserManaL, gbc_UserManaL);

		atk1Button = new JButton("Weak attack");
		AttackButtonHandler atk1Handler = new AttackButtonHandler();
		atk1Handler.doActionType(1);
		atk1Button.addActionListener(atk1Handler);
		GridBagConstraints gbc_atk1Button = new GridBagConstraints();
		gbc_atk1Button.insets = new Insets(0, 0, 5, 5);
		gbc_atk1Button.gridx = 10;
		gbc_atk1Button.gridy = 6;
		getContentPane().add(atk1Button, gbc_atk1Button);

		atk2Button = new JButton("Strong attack");
		AttackButtonHandler atk2Handler = new AttackButtonHandler();
		atk2Handler.doActionType(2);
		atk2Button.addActionListener(atk2Handler);
		GridBagConstraints gbc_atk2Button = new GridBagConstraints();
		gbc_atk2Button.insets = new Insets(0, 0, 5, 5);
		gbc_atk2Button.gridx = 10;
		gbc_atk2Button.gridy = 7;
		getContentPane().add(atk2Button, gbc_atk2Button);

		atk3Button = new JButton("Fire Ball");
		AttackButtonHandler atk3Handler = new AttackButtonHandler();
		atk3Handler.doActionType(3);
		atk3Button.addActionListener(atk3Handler);
		GridBagConstraints gbc_atk3Button = new GridBagConstraints();
		gbc_atk3Button.insets = new Insets(0, 0, 5, 5);
		gbc_atk3Button.gridx = 10;
		gbc_atk3Button.gridy = 8;
		getContentPane().add(atk3Button, gbc_atk3Button);

		exitButton = new JButton("Close Game");
		ExitButtonHandler ebhandler = new ExitButtonHandler();
		exitButton.addActionListener(ebhandler);
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 10;
		gbc_button.gridy = 9;
		getContentPane().add(exitButton, gbc_button);

	}

	private class AttackButtonHandler implements ActionListener {
		private int atkType = 0;

		public void doActionType(int x) {
			atkType = x;
		}

		public void actionPerformed(ActionEvent e) {
			if (BossIsAlive) {
				switch (atkType) {
				case 1:
					EnemyHP -= 10;
					Animate();
					break;
				case 2:
					EnemyHP -= 30;
					Animate();
					break;
				case 3:
					EnemyHP -= 10;
					enemyBurn = 3;
					UserMana -= 30;
					System.out.println("Enemy is burnt");
					Animate();
					break;
				default:
					System.out.println("An invalid atk has occured");
					break;
				}

				if (enemyBurn > 0) {
					EnemyHP -= 10;
					System.out.println("Enemy took burn damage");
					enemyBurn -= 1;
				}
				enemyHPL.setText(" HP: " + EnemyHP);
				UserManaL.setText(" Mana: " + UserMana);
				if (EnemyHP < 1) {
					System.out.println("The Battle is finished");
					Win();
					BossIsAlive = false;
				}
				DemonAI.DemonAttack();
			}
		}
	}

	public class ExitButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	}

	public void Win() {
		winL = new JLabel("YOU DEFEATED THE BOSS AND WON THE BATTLE", SwingConstants.CENTER);
		Container pane = getContentPane();
		pane.repaint();
		pane.removeAll();
		pane.setLayout(new GridLayout(1, 1));
		pane.add(winL);
		pane.revalidate();

	}

	public void Animate() {
		if (!AnimationOn) {
			Container pane = getContentPane();
			timer = new Timer(100, new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					enemySpriteL
							.setIcon(flag ? new ImageIcon("Images/Demon1.png") : new ImageIcon("Images/Demon2.png"));
					flag = !flag;
					pane.repaint();
					y++;
					if (y == 4) {
						timer.stop();
						y = 0;
						AnimationOn = false;
					}
				}
			});
			timer.start();
			AnimationOn = true;
		}
	}
}